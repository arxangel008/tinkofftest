package com.tinkoff;

import com.tinkoff.feign.TranslateFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableFeignClients(basePackageClasses = TranslateFeign.class)
public class TinkoffTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TinkoffTestApplication.class, args);
    }
}
