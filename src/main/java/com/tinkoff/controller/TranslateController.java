package com.tinkoff.controller;

import com.tinkoff.dto.RequestDto;
import com.tinkoff.service.TranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class TranslateController {

    private final TranslateService translateService;

    @Autowired
    public TranslateController(TranslateService translateService) {
        this.translateService = translateService;
    }

    @RequestMapping(value = "/translate", method = RequestMethod.GET)
    public String getTranslate(@ModelAttribute RequestDto requestDto) {
        return translateService.getTranslate(requestDto);
    }
}
