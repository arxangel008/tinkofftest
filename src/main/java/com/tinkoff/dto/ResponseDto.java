package com.tinkoff.dto;

import lombok.*;

/**
 * Класс итогового перевода
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Setter
@Getter
public class ResponseDto {

    /**
     * Текст после перевода
     */
    String text;

    public void setText(String[] text) {
        this.text = text[0];
    }
}
