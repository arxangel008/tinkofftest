package com.tinkoff.dto;

import lombok.*;

/**
 * Класс параметров запроса перевода
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Setter
@Getter
public class RequestDto {

    /**
     * Переводимый текст
     */
    private String text;

    /**
     * Язык исходного текста
     */
    private String from;

    /**
     * Язык итогового текста
     */
    private String to;
}
